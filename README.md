# secret-rule-engine

#### 介绍
- 基于drools实现的动态规则引擎，只实现核心代码，根据条件自由组合配置动态生成drools脚本以及脚本测试
- 根据实际项目需要来调整rule_condition_config表里的条件字段
- 执行TestController 中的 generationDrools为动态生成规则脚本接口
- 执行testRule 进行规则脚本测试

| 字段类型 | |
| ----- | -----|
| String | 字符串 |
| Decimal | 数值 |
| custom | 自定义 |
| Other | 其他 |

| 条件 | |
| ----- | -----|
| == | 等于 |
| != | 不等于 |
| contains | 包含 |
| &gt; | 大于 |
| &lt; | 小于 |
| &gt;= | 大于等于 |
| &lt;= | 小于等于 |
