package com.secretopen.ruleengine.service;

import com.secretopen.ruleengine.entity.*;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author secret
 * @since 2023-09-09
 */
public interface RuleDroolsService extends IService<RuleDrools> {

    void generationDrools(List<RuleApplyConfig> ruleApplyConfigs);

    String ruleToDrl(RuleApplyConfig ruleApplyConfig,
                     List<RuleConditionConfig> ruleConditionConfigs);

    List<RuleResult> executeDrools(List<RuleDrools> ruleDrools, CaseInfoFact caseInfoFact);
}
