package com.secretopen.ruleengine.service.impl;

import com.secretopen.ruleengine.entity.RuleConditionConfig;
import com.secretopen.ruleengine.mapper.RuleConditionConfigMapper;
import com.secretopen.ruleengine.service.RuleConditionConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 规则条件配置 服务实现类
 * </p>
 *
 * @author secret
 * @since 2023-09-09
 */
@Service
public class RuleConditionConfigServiceImpl extends ServiceImpl<RuleConditionConfigMapper, RuleConditionConfig> implements RuleConditionConfigService {

}
