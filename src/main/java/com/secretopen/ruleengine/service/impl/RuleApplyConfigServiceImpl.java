package com.secretopen.ruleengine.service.impl;

import com.secretopen.ruleengine.entity.RuleApplyConfig;
import com.secretopen.ruleengine.mapper.RuleApplyConfigMapper;
import com.secretopen.ruleengine.service.RuleApplyConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 规则应用配置 服务实现类
 * </p>
 *
 * @author secret
 * @since 2023-09-09
 */
@Service
public class RuleApplyConfigServiceImpl extends ServiceImpl<RuleApplyConfigMapper, RuleApplyConfig> implements RuleApplyConfigService {

}
