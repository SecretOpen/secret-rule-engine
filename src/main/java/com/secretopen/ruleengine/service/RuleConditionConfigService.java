package com.secretopen.ruleengine.service;

import com.secretopen.ruleengine.entity.RuleConditionConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 规则条件配置 服务类
 * </p>
 *
 * @author secret
 * @since 2023-09-09
 */
public interface RuleConditionConfigService extends IService<RuleConditionConfig> {

}
