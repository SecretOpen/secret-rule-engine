package com.secretopen.ruleengine.service;

import com.secretopen.ruleengine.entity.RuleApplyConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 规则应用配置 服务类
 * </p>
 *
 * @author secret
 * @since 2023-09-09
 */
public interface RuleApplyConfigService extends IService<RuleApplyConfig> {

}
