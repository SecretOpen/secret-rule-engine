package com.secretopen.ruleengine.entity;

import java.util.Date;

public class CaseInfoFact {
	private String caseNo;// 报案号
	private String caseReportDt; // 报案时间
	private String notifyTelephone; // 报案电话
	private String notifyMan; // 报案人姓名
	private String driver; // 驾驶员姓名
	private String caseDate; // 出险时间
	private String lossType; // 出险原因
	private String damageCase; // 出险情况
	private String damageAddress; // 出险地址
	private String isUnilateral; // 单方事故/双方事故
	private String fastCase; // 是否快撤案件
//	private String ifLitigation; // 是否诉讼
//	private String ifPersonLoss; // 是否人伤
	private String sourceFlag; // 案件来源标志,1--P17,2--M6,3--天眼
	private String status; // 案件处理状态,0--未处理，1--处理中，2--已处理完
	private Date insertTime; // 案件插入时间
	private String notifyIdentity;// 报案人身份
	private String driverIdentity;// 驾驶员身份
	private String carOwner; // 车主
	private Date updateTime;// 更新时间
	private String department; // 处理事故部门
	private String damageAddressCompanyCode; // 出险地分公司代码
	private String damageAddressCompanyName; // 出险地分公司名称
	private String damageAddressProvince; // 出险地（省）
	private String damageAddressDistrict; // 出险地（市）
	private String vehicleActualPrice; // 标的车车辆承保实际价值
	private String vehicleServiceLife; // 标的车车辆使用年限
	private String vehicleInitalDate; // 标的车车辆初登日期
	private String moldName; // 标的车车型名称
	private String effectiveDate; // 承保开始日期
	private String expireDate; // 承保结束日期
	private String insurerCode; // 承保机构代码
	private String insurerName; // 承保机构
	private String vehicleNo; // 车牌号
	private String insurantName; // 被保险人姓名
	private String usage; // 使用性质
//	private String newConversionFlag; // 新转续标记
//	private String vin; // 车架号
	private String accidentCaseCount; // 出险次数
//	private String policyNo;// 保单号
//	private String totalPremium;// 总保费
//	private String totalInsuredAmount;// 总保额
//	private String beneficiary;// 索赔权益人
	private String brandName;// 车型信息
	private String seatCount; // 车辆座位数
	private String transferringFlag; // 续保
	// 保险公司代码
	private String insuranceCompanyCode;
	// 保险公司名称
	private String insuranceCompanyName;
	private String policyTypes;// 承保类型

	private String coverageCodes; // 条款代码
	private String coverageNames; // 条款名称

	public String getCaseNo() {
		return caseNo;
	}

	public void setCaseNo(String caseNo) {
		this.caseNo = caseNo;
	}

	public String getCaseReportDt() {
		return caseReportDt;
	}

	public void setCaseReportDt(String caseReportDt) {
		this.caseReportDt = caseReportDt;
	}

	public String getNotifyTelephone() {
		return notifyTelephone;
	}

	public void setNotifyTelephone(String notifyTelephone) {
		this.notifyTelephone = notifyTelephone;
	}

	public String getNotifyMan() {
		return notifyMan;
	}

	public void setNotifyMan(String notifyMan) {
		this.notifyMan = notifyMan;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getCaseDate() {
		return caseDate;
	}

	public void setCaseDate(String caseDate) {
		this.caseDate = caseDate;
	}

	public String getLossType() {
		return lossType;
	}

	public void setLossType(String lossType) {
		this.lossType = lossType;
	}

	public String getDamageCase() {
		return damageCase;
	}

	public void setDamageCase(String damageCase) {
		this.damageCase = damageCase;
	}

	public String getDamageAddress() {
		return damageAddress;
	}

	public void setDamageAddress(String damageAddress) {
		this.damageAddress = damageAddress;
	}

	public String getIsUnilateral() {
		return isUnilateral;
	}

	public void setIsUnilateral(String isUnilateral) {
		this.isUnilateral = isUnilateral;
	}

	public String getFastCase() {
		return fastCase;
	}

	public void setFastCase(String fastCase) {
		this.fastCase = fastCase;
	}

	public String getSourceFlag() {
		return sourceFlag;
	}

	public void setSourceFlag(String sourceFlag) {
		this.sourceFlag = sourceFlag;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	public String getNotifyIdentity() {
		return notifyIdentity;
	}

	public void setNotifyIdentity(String notifyIdentity) {
		this.notifyIdentity = notifyIdentity;
	}

	public String getDriverIdentity() {
		return driverIdentity;
	}

	public void setDriverIdentity(String driverIdentity) {
		this.driverIdentity = driverIdentity;
	}

	public String getCarOwner() {
		return carOwner;
	}

	public void setCarOwner(String carOwner) {
		this.carOwner = carOwner;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getDamageAddressCompanyCode() {
		return damageAddressCompanyCode;
	}

	public void setDamageAddressCompanyCode(String damageAddressCompanyCode) {
		this.damageAddressCompanyCode = damageAddressCompanyCode;
	}

	public String getDamageAddressCompanyName() {
		return damageAddressCompanyName;
	}

	public void setDamageAddressCompanyName(String damageAddressCompanyName) {
		this.damageAddressCompanyName = damageAddressCompanyName;
	}

	public String getDamageAddressProvince() {
		return damageAddressProvince;
	}

	public void setDamageAddressProvince(String damageAddressProvince) {
		this.damageAddressProvince = damageAddressProvince;
	}

	public String getDamageAddressDistrict() {
		return damageAddressDistrict;
	}

	public void setDamageAddressDistrict(String damageAddressDistrict) {
		this.damageAddressDistrict = damageAddressDistrict;
	}

	public String getVehicleActualPrice() {
		return vehicleActualPrice;
	}

	public void setVehicleActualPrice(String vehicleActualPrice) {
		this.vehicleActualPrice = vehicleActualPrice;
	}

	public String getVehicleServiceLife() {
		return vehicleServiceLife;
	}

	public void setVehicleServiceLife(String vehicleServiceLife) {
		this.vehicleServiceLife = vehicleServiceLife;
	}

	public String getVehicleInitalDate() {
		return vehicleInitalDate;
	}

	public void setVehicleInitalDate(String vehicleInitalDate) {
		this.vehicleInitalDate = vehicleInitalDate;
	}

	public String getMoldName() {
		return moldName;
	}

	public void setMoldName(String moldName) {
		this.moldName = moldName;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public String getInsurerCode() {
		return insurerCode;
	}

	public void setInsurerCode(String insurerCode) {
		this.insurerCode = insurerCode;
	}

	public String getInsurerName() {
		return insurerName;
	}

	public void setInsurerName(String insurerName) {
		this.insurerName = insurerName;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getInsurantName() {
		return insurantName;
	}

	public void setInsurantName(String insurantName) {
		this.insurantName = insurantName;
	}

	public String getUsage() {
		return usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

	public String getAccidentCaseCount() {
		return accidentCaseCount;
	}

	public void setAccidentCaseCount(String accidentCaseCount) {
		this.accidentCaseCount = accidentCaseCount;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getSeatCount() {
		return seatCount;
	}

	public void setSeatCount(String seatCount) {
		this.seatCount = seatCount;
	}

	public String getTransferringFlag() {
		return transferringFlag;
	}

	public String getPolicyTypes() {
		return policyTypes;
	}

	public void setPolicyTypes(String policyTypes) {
		this.policyTypes = policyTypes;
	}

	public String getCoverageCodes() {
		return coverageCodes;
	}

	public void setCoverageCodes(String coverageCodes) {
		this.coverageCodes = coverageCodes;
	}

	public String getCoverageNames() {
		return coverageNames;
	}

	public void setCoverageNames(String coverageNames) {
		this.coverageNames = coverageNames;
	}

	public void setTransferringFlag(String transferringFlag) {
		this.transferringFlag = transferringFlag;
	}

	public String getInsuranceCompanyCode() {
		return insuranceCompanyCode;
	}

	public void setInsuranceCompanyCode(String insuranceCompanyCode) {
		this.insuranceCompanyCode = insuranceCompanyCode;
	}

	public String getInsuranceCompanyName() {
		return insuranceCompanyName;
	}

	public void setInsuranceCompanyName(String insuranceCompanyName) {
		this.insuranceCompanyName = insuranceCompanyName;
	}

	@Override
	public String toString() {
		return "CaseInfoFact [caseNo=" + caseNo + ", caseReportDt=" + caseReportDt + ", notifyTelephone="
				+ notifyTelephone + ", notifyMan=" + notifyMan + ", driver=" + driver + ", caseDate=" + caseDate
				+ ", lossType=" + lossType + ", damageCase=" + damageCase + ", damageAddress=" + damageAddress
				+ ", isUnilateral=" + isUnilateral + ", fastCase=" + fastCase + ", sourceFlag=" + sourceFlag
				+ ", status=" + status + ", insertTime=" + insertTime + ", notifyIdentity=" + notifyIdentity
				+ ", driverIdentity=" + driverIdentity + ", carOwner=" + carOwner + ", updateTime=" + updateTime
				+ ", department=" + department + ", damageAddressCompanyCode=" + damageAddressCompanyCode
				+ ", damageAddressCompanyName=" + damageAddressCompanyName + ", damageAddressProvince="
				+ damageAddressProvince + ", damageAddressDistrict=" + damageAddressDistrict + ", vehicleActualPrice="
				+ vehicleActualPrice + ", vehicleServiceLife=" + vehicleServiceLife + ", vehicleInitalDate="
				+ vehicleInitalDate + ", moldName=" + moldName + ", effectiveDate=" + effectiveDate + ", expireDate="
				+ expireDate + ", insurerCode=" + insurerCode + ", insurerName=" + insurerName + ", vehicleNo="
				+ vehicleNo + ", insurantName=" + insurantName + ", usage=" + usage + ", accidentCaseCount="
				+ accidentCaseCount + ", brandName=" + brandName + ", seatCount=" + seatCount + ", transferringFlag="
				+ transferringFlag + ", insuranceCompanyCode=" + insuranceCompanyCode + ", insuranceCompanyName="
				+ insuranceCompanyName + ", policyTypes=" + policyTypes + ", coverageCodes=" + coverageCodes
				+ ", coverageNames=" + coverageNames + "]";
	}


}
