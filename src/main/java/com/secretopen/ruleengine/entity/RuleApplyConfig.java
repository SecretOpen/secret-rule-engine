package com.secretopen.ruleengine.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 规则应用配置
 * </p>
 *
 * @author secret
 * @since 2023-09-09
 */
@Getter
@Setter
@TableName("rule_apply_config")
public class RuleApplyConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id，自增
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 类型code
     */
    @TableField("type_code")
    private String typeCode;

    /**
     * 类型name
     */
    @TableField("type_name")
    private String typeName;

    /**
     * 是否有效，1--有效，0--无效
     */
    @TableField("use_status")
    private String useStatus;

    /**
     * 有效开始日期
     */
    @TableField("effective_start_date")
    private Date effectiveStartDate;

    /**
     * 有效结束日期
     */
    @TableField("effective_end_date")
    private Date effectiveEndDate;

    /**
     * 表达式
     */
    @TableField("expression")
    private String expression;

    /**
     * 表达式条件集合
     */
    @TableField("expression_ids")
    private String expressionIds;

    /**
     * 插入时间
     */
    @TableField("insert_time")
    private Date insertTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;


}
