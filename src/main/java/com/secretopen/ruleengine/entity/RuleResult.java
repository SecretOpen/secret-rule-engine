package com.secretopen.ruleengine.entity;

/**
 * 规则结果输出
 *
 * @author Andy
 *
 */

public class RuleResult {
	private String caseNo;
	private String foreignId;
	private String ruleId;
	private String ruleTip;
	private String ruleName;
	private String ruleKey;

	public String getCaseNo() {
		return caseNo;
	}

	public void setCaseNo(String caseNo) {
		this.caseNo = caseNo;
	}

	public String getForeignId() {
		return foreignId;
	}

	public void setForeignId(String foreignId) {
		this.foreignId = foreignId;
	}

	public String getRuleId() {
		return ruleId;
	}

	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}

	public String getRuleTip() {
		return ruleTip;
	}

	public void setRuleTip(String ruleTip) {
		this.ruleTip = ruleTip;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getRuleKey() {
		return ruleKey;
	}

	public void setRuleKey(String ruleKey) {
		this.ruleKey = ruleKey;
	}



}
