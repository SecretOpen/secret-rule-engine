package com.secretopen.ruleengine.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author secret
 * @since 2023-09-09
 */
@Getter
@Setter
@TableName("rule_drools")
public class RuleDrools implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("business_id")
    private String businessId;

    /**
     * drools内容
     */
    @TableField("content")
    private String content;

    /**
     * 外键（应用表）
     */
    @TableField("foreign_id")
    private String foreignId;

    /**
     * 执行类型
     */
    @TableField("drools_type")
    private String droolsType;

    /**
     * 是否有效，1--有效，0--无效
     */
    @TableField("drools_status")
    private String droolsStatus;

    /**
     * 插入时间
     */
    @TableField("insert_time")
    private Date insertTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;


}
