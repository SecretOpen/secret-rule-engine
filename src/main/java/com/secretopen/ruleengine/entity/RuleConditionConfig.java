package com.secretopen.ruleengine.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 规则条件配置
 * </p>
 *
 * @author secret
 * @since 2023-09-09
 */
@Getter
@Setter
@TableName("rule_condition_config")
public class RuleConditionConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 字段code
     */
    @TableField("field_code")
    private String fieldCode;

    /**
     * 字段名称
     */
    @TableField("field_name")
    private String fieldName;

    /**
     * 字段类型
     */
    @TableField("field_type")
    private String fieldType;

    /**
     * 条件code
     */
    @TableField("condition_code")
    private String conditionCode;

    /**
     * 条件名称
     */
    @TableField("condition_name")
    private String conditionName;

    /**
     * 值
     */
    @TableField("field_value")
    private String fieldValue;

    /**
     * 提示信息
     */
    @TableField("tooltips")
    private String tooltips;

    /**
     * 插入时间
     */
    @TableField("insert_time")
    private Date insertTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 创建人code
     */
    @TableField("create_user_code")
    private String createUserCode;

    /**
     * 创建人名称
     */
    @TableField("create_user_name")
    private String createUserName;

    /**
     * 修改人code
     */
    @TableField("update_user_code")
    private String updateUserCode;

    /**
     * 修改人名称
     */
    @TableField("update_user_name")
    private String updateUserName;


}
