package com.secretopen.ruleengine.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author secret
 * @since 2023-09-09
 */
@RestController
@RequestMapping("/ruleDrools")
public class RuleDroolsController {

}

