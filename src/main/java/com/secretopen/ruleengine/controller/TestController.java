package com.secretopen.ruleengine.controller;

import cn.chenc.framework.core.model.response.AjaxResult;
import cn.chenc.framework.core.text.Convert;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.secretopen.ruleengine.entity.CaseInfoFact;
import com.secretopen.ruleengine.entity.RuleApplyConfig;
import com.secretopen.ruleengine.entity.RuleConditionConfig;
import com.secretopen.ruleengine.entity.RuleDrools;
import com.secretopen.ruleengine.service.RuleApplyConfigService;
import com.secretopen.ruleengine.service.RuleDroolsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 　@description: TODO
 * 　@author secret
 * 　@date 2023/9/9 16:13
 *
 */
@RestController
public class TestController {

    @Autowired
    private RuleApplyConfigService ruleApplyConfigService;
    @Autowired
    private RuleDroolsService ruleDroolsService;

    @GetMapping("/test")
    public AjaxResult test(){
        return AjaxResult.success();
    }

    @GetMapping("/generationDrools")
    public AjaxResult generationDrools() {
        List<RuleApplyConfig> ruleApplyConfigs = ruleApplyConfigService.list();
        ruleDroolsService.generationDrools(ruleApplyConfigs);
        return AjaxResult.success();
    }

    @GetMapping("/testRule")
    public AjaxResult testRule() {
        List<RuleDrools> ruleDrools=new ArrayList<RuleDrools>();
        RuleDrools ruDrools=new RuleDrools();
        String content="package com.secretopen.ruledrools;\n" +
                "import com.secretopen.ruleengine.entity.RuleResult;\n" +
                "import com.secretopen.ruleengine.entity.CaseInfoFact;\n" +
                "import cn.hutool.core.date.DateUnit;\n" +
                "import cn.hutool.core.date.DateUtil;\n" +
                "global java.util.List resultList;\n" +
                "\n" +
                "rule \"10013_组合规则1\"\n" +
                "\twhen\n" +
                "\t\t$caseInfoFact : CaseInfoFact((lossType contains \"玻璃单独破碎\" " +
                "|| lossType contains \"空中分型物体坠落\")" +
                "||(damageAddress contains \"高速\" " +
                "|| damageAddress contains \"G\")" +
                "||(damageCase contains \"取车\" " +
                "|| damageCase contains \"停放\" " +
                "|| damageCase contains \"狗\" " +
                "|| damageCase contains \"坠\")" +
                "||coverageNames not contains \"机动车损失保险\");\n" +
                "\tthen\n" +
                "\t\tRuleResult ruleResult=new RuleResult();\n" +
                "\t\truleResult.setCaseNo($caseInfoFact.getCaseNo()); \n" +
                "\t\truleResult.setForeignId(\"10013\");\n" +
                "\t\truleResult.setRuleKey(\"01\");\n" +
                "\t\truleResult.setRuleName(\"组合规则1\");\n" +
                "\t\tresultList.add(ruleResult);\n" +
                "end\n";

        ruDrools.setContent(content);
        ruleDrools.add(ruDrools);

        CaseInfoFact caseInfoFact=getCaseInfoFact();

        ruleDroolsService.executeDrools(ruleDrools, caseInfoFact);
//		 CaseReportDt  CaseDate
        String caseDate="2021-10-22 23:14:21";
        String caseReportDt="2021-10-22 22:12:21";
        long hour= DateUtil.between(DateUtil.parseDateTime(caseDate), DateUtil.parseDateTime(caseReportDt), DateUnit.HOUR);

//		(DateUtil.hour(DateUtil.parseDateTime(caseDate), true)>4 && DateUtil.hour(DateUtil.parseDateTime(caseDate), true)<20)

        System.out.println("ceshi:"+hour);
        return AjaxResult.success();
    }

    private static CaseInfoFact getCaseInfoFact() {
        CaseInfoFact caseInfoFact=new CaseInfoFact();
        caseInfoFact.setCaseNo("qwer");
        caseInfoFact.setDamageAddress("广东省广州市xxxxxx");
        caseInfoFact.setDamageCase("非现场案件，标的车xxx碰撞石墩，导致标的车受损，物损无需赔付请配合查勘处理。");
        caseInfoFact.setDriver("*xx");
        caseInfoFact.setNotifyMan("*xx");
        caseInfoFact.setCaseNo("qwer");
        caseInfoFact.setUsage("家庭自用车");
        caseInfoFact.setLossType("碰撞");
        caseInfoFact.setIsUnilateral("02");

        caseInfoFact.setInsurantName("qwer");
//		caseInfoFact.setPolicyTypes("01,02");
        caseInfoFact.setCaseReportDt("2023-09-05 15:05:04");
        caseInfoFact.setCaseDate("2023-09-05 02:00:00");
        caseInfoFact.setCoverageNames("交强险");

        return caseInfoFact;
    }

}
