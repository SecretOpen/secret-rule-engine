package com.secretopen.ruleengine;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.engine.VelocityTemplateEngine;

import java.sql.Types;
import java.util.Collections;

/**
 * 　@description: TODO
 * 　@author secret
 * 　@date 2023/9/9 16:18
 *
 */

public class MybatisPlusGeneration {

    public static void main(String[] args) {

        String outputDir = System.getProperty("user.dir") + "/src/main/java";
        FastAutoGenerator.create("jdbc:mysql://127.0.0.1:3306/secret_rule_engine?useUnicode=true&characterEncoding=utf-8&useSSL=false&serverTimezone=GMT&rewriteBatchedStatements=true"
                        , "root", "111111")//数据库配置
                .globalConfig(builder -> {
                    builder.author("secret") // 设置作者
//                            .enableSwagger() // 开启 swagger 模式
                            .fileOverride() // 覆盖已生成文件
                            .dateType(DateType.ONLY_DATE)// 日期类型
                            .outputDir(outputDir); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.secretopen.ruleengine") // 设置父包名
                            //.moduleName("sys") // 设置父包模块名
                            .controller("controller")
                            .mapper("mapper")
                            .service("service")
                            .serviceImpl("service.impl")
                            .entity("entity")
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, outputDir)); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude("rule_apply_config","rule_condition_config","rule_drools") // 设置需要生成的表名
//                            .addTablePrefix("test_") // 设置过滤表前缀
                            //entity配置
                            .entityBuilder()
                            .enableLombok()
                            //.superClass(com.cloud.model.common.BaseDataEntity.class)//继承父类
                            //.logicDeleteColumnName("del_flag")//逻辑删除字段
                            .enableTableFieldAnnotation()
                            //controller配置
                            .controllerBuilder()
                            .formatFileName("%sController")
                            .enableRestStyle()
                            //service配置
                            .serviceBuilder()
                            .formatServiceFileName("%sService")
                            .formatServiceImplFileName("%sServiceImpl")
                            //dao配置
                            .mapperBuilder()
                            .formatMapperFileName("%sMapper")
                            .formatXmlFileName("%sMapper")
                            .enableBaseResultMap()
                            .enableBaseColumnList()
                            .enableMapperAnnotation();
                })
                //模板配置
                .templateConfig((scanner, builder) -> builder
                                .build()//加入构建队列
                )
                .injectionConfig(builder -> builder
                        .beforeOutputFile((tableInfo, objectMap) -> {
                            System.out.println("tableInfo: " + tableInfo.getEntityName() + " objectMap: " + objectMap.size());
                        }) //输出文件之前消费者
                        .build()//加入构建队列
                )
                // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .templateEngine(new VelocityTemplateEngine())
                .execute();


    }
}
