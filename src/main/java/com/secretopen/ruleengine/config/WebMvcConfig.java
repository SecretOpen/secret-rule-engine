package com.secretopen.ruleengine.config;

import cn.chenc.framework.core.filter.XssFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.HashMap;
import java.util.Map;

/**
 * 　@description: TODO
 * 　@author secret
 * 　@date 2023/9/9 16:11
 *
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    /**
     * 代码方式注册过滤器Bean
     * @return
     */
    @Bean
    public FilterRegistrationBean xssFilterRegistration(){
        FilterRegistrationBean filterBean = new FilterRegistrationBean();
        filterBean.setFilter(new XssFilter());
        filterBean.setName("xssFilter");
        filterBean.addUrlPatterns("/*");
        Map<String, String> initParameters = new HashMap<String, String>();
//        initParameters.put("excludes", null);
//        initParameters.put("enabled", null);
        filterBean.setInitParameters(initParameters);
        return filterBean;
    }


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/static/");
    }

}
