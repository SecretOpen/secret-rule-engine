package com.secretopen.ruleengine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 　@description: TODO
 * 　@author secret
 * 　@date 2023/9/9 15:37
 *
 */
@SpringBootApplication
@EnableTransactionManagement
public class RuleEngineApplication {

    public static void main(String[] args){
        SpringApplication.run(RuleEngineApplication.class, args);
    }

}
