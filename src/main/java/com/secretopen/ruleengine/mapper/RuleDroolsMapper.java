package com.secretopen.ruleengine.mapper;

import com.secretopen.ruleengine.entity.RuleDrools;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author secret
 * @since 2023-09-09
 */
@Mapper
public interface RuleDroolsMapper extends BaseMapper<RuleDrools> {

}
