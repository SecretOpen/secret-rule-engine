package com.secretopen.ruleengine.mapper;

import com.secretopen.ruleengine.entity.RuleConditionConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 规则条件配置 Mapper 接口
 * </p>
 *
 * @author secret
 * @since 2023-09-09
 */
@Mapper
public interface RuleConditionConfigMapper extends BaseMapper<RuleConditionConfig> {

}
