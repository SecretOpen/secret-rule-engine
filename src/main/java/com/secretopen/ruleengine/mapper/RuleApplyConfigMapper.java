package com.secretopen.ruleengine.mapper;

import com.secretopen.ruleengine.entity.RuleApplyConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 规则应用配置 Mapper 接口
 * </p>
 *
 * @author secret
 * @since 2023-09-09
 */
@Mapper
public interface RuleApplyConfigMapper extends BaseMapper<RuleApplyConfig> {

}
