package com.secretopen.ruleengine.consts;

import java.util.HashMap;
import java.util.Map;

public class Constants {

	public static final String FIELD_TYPE_STRING = "String"; //字符串
	public static final String FIELD_TYPE_DECIMAL = "Decimal"; //数值
	public static final String FIELD_TYPE_CUSTOM = "custom"; //自定义
	public static final String FIELD_TYPE_OTHER = "Other"; //其他

}
