/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50720
 Source Host           : localhost:3306
 Source Schema         : secret_rule_engine

 Target Server Type    : MySQL
 Target Server Version : 50720
 File Encoding         : 65001

 Date: 09/09/2023 18:15:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for rule_apply_config
-- ----------------------------
DROP TABLE IF EXISTS `rule_apply_config`;
CREATE TABLE `rule_apply_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id，自增',
  `type_code` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型code，01--，02--，03--，04-- ，05--',
  `type_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型name',
  `use_status` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否有效，1--有效，0--无效',
  `effective_start_date` datetime(0) NULL DEFAULT NULL COMMENT '有效开始日期',
  `effective_end_date` datetime(0) NULL DEFAULT NULL COMMENT '有效结束日期',
  `expression` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表达式',
  `expression_ids` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表达式条件集合',
  `insert_time` datetime(0) NULL DEFAULT NULL COMMENT '插入时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10017 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '规则应用配置' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rule_apply_config
-- ----------------------------
INSERT INTO `rule_apply_config` VALUES (10013, '01', '组合规则1', '1', NULL, NULL, '10001||10002||10003||10004', '10001,10002,10003,10004', '2021-11-03 15:41:42', '2021-11-03 16:08:28', NULL);
INSERT INTO `rule_apply_config` VALUES (10014, '02', '组合规则2', '1', NULL, NULL, '10005&&10006&&10007&&10008', '10005,10006,10007,10008', '2021-11-04 14:41:46', '2021-11-04 16:40:30', NULL);
INSERT INTO `rule_apply_config` VALUES (10015, '03', '组合规则3', '1', NULL, NULL, '10005&&10006&&10009&&10010&&10011&&10012', '10005,10006,10009,10010,10011,10012', '2021-11-04 17:03:31', '2021-11-04 17:03:31', NULL);
INSERT INTO `rule_apply_config` VALUES (10016, '04', '组合规则4', '1', NULL, NULL, '10006&&10011&&10013&&10014&&(10015||10016)', '10006,10011,10013,10014,10015,10016', '2021-11-04 17:46:01', '2021-11-04 17:46:01', NULL);

-- ----------------------------
-- Table structure for rule_condition_config
-- ----------------------------
DROP TABLE IF EXISTS `rule_condition_config`;
CREATE TABLE `rule_condition_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `field_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段code',
  `field_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名称',
  `field_type` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段类型',
  `condition_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '条件code',
  `condition_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '条件名称',
  `field_value` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '值',
  `tooltips` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '提示信息',
  `insert_time` datetime(0) NULL DEFAULT NULL COMMENT '插入时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `create_user_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人code',
  `create_user_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `update_user_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人code',
  `update_user_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10017 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '规则条件配置' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rule_condition_config
-- ----------------------------
INSERT INTO `rule_condition_config` VALUES (10001, 'lossType', '出险原因', 'String', 'contains', '包含', '玻璃单独破碎or空中分型物体坠落', '出险原因包含玻璃单独破碎或空中分型物体坠落', '2021-11-03 10:20:07', '2021-11-03 10:20:07', 'admin', '管理员2', NULL, NULL);
INSERT INTO `rule_condition_config` VALUES (10002, 'damageAddress', '出险地址', 'String', 'contains', '包含', '高速orG', '出险地址包含高速或G', '2021-11-03 10:25:26', '2021-11-03 10:28:34', 'admin', '管理员2', 'admin', '管理员2');
INSERT INTO `rule_condition_config` VALUES (10003, 'damageCase', '出险情况', 'String', 'contains', '包含', '取车or停放or狗or坠', '出险情况包含取车、或停放、或狗、或坠', '2021-11-03 10:26:34', '2021-11-03 10:28:15', 'admin', '管理员2', 'admin', '管理员2');
INSERT INTO `rule_condition_config` VALUES (10004, 'coverageNames', '条款名称', 'String', 'not contains', '不包含', '机动车损失保险', '险别险种不包含机动车损失保险', '2021-11-03 10:27:42', '2021-11-03 10:28:38', 'admin', '管理员2', 'admin', '管理员2');
INSERT INTO `rule_condition_config` VALUES (10005, 'lossType', '出险原因', 'String', '==', '等于', '碰撞or坠落or倾覆', '出险原因	等于	碰撞/坠落/倾覆', '2021-11-03 17:43:12', '2021-11-03 17:43:12', 'admin', '管理员2', NULL, NULL);
INSERT INTO `rule_condition_config` VALUES (10006, 'usage', '使用性质', 'String', '==', '等于', '02', '使用性质	等于	非营运', '2021-11-03 17:44:28', '2021-11-04 15:56:25', 'admin', '管理员2', 'admin', '管理员2');
INSERT INTO `rule_condition_config` VALUES (10007, 'driver', '驾驶员姓名', 'Other', '!=', '不等于', 'insurantName', '驾驶员 不等于 被保险人', '2021-11-03 17:45:50', '2021-11-03 17:45:50', 'admin', '管理员2', NULL, NULL);
INSERT INTO `rule_condition_config` VALUES (10008, 'custom', '自定义', 'custom', 'not contains', '不包含', '!(DateUtil.hour(DateUtil.parseDateTime(caseDate), true)>4 && DateUtil.hour(DateUtil.parseDateTime(caseDate), true)<20)', '出险时间	属于	晚8点到早4点', '2021-11-04 14:39:01', '2021-11-04 14:39:01', 'admin', '管理员2', NULL, NULL);
INSERT INTO `rule_condition_config` VALUES (10009, 'custom', '自定义', 'custom', 'not contains', '不包含', '!(DateUtil.hour(DateUtil.parseDateTime(caseDate), true)>5 && DateUtil.hour(DateUtil.parseDateTime(caseDate), true)<21)', '出险时间	属于	晚9点到早5点', '2021-11-04 16:53:40', '2021-11-04 16:53:40', 'admin', '管理员2', NULL, NULL);
INSERT INTO `rule_condition_config` VALUES (10010, 'driver', '驾驶员姓名', 'Other', '==', '等于', 'notifyMan', '驾驶员 等于报案人', '2021-11-04 16:55:44', '2021-11-04 16:55:44', 'admin', '管理员2', NULL, NULL);
INSERT INTO `rule_condition_config` VALUES (10011, 'isUnilateral', '是否单方事故', 'String', '==', '等于', '1', '事故类型等于单方事故', '2021-11-04 16:57:08', '2021-11-04 16:57:08', 'admin', '管理员2', NULL, NULL);
INSERT INTO `rule_condition_config` VALUES (10012, 'custom', '自定义', 'custom', '>', '大于', 'DateUtil.between(DateUtil.parseDateTime(caseDate), DateUtil.parseDateTime(caseReportDt), DateUnit.HOUR)>=1', '报案时间大于出险时间1小时以上', '2021-11-04 17:00:01', '2021-11-04 17:23:49', 'admin', '管理员2', 'admin', '管理员2');
INSERT INTO `rule_condition_config` VALUES (10013, 'lossType', '出险原因', 'String', '==', '等于', '碰撞', '出险原因属于碰撞', '2021-11-04 17:32:49', '2021-11-04 17:32:49', 'admin', '管理员2', NULL, NULL);
INSERT INTO `rule_condition_config` VALUES (10014, 'vehicleServiceLife', '标的车车辆使用年限', 'String', '>', '大于', '1', '车辆使用年限大于1年', '2021-11-04 17:34:19', '2021-11-04 17:34:19', 'admin', '管理员2', NULL, NULL);
INSERT INTO `rule_condition_config` VALUES (10015, 'custom', '自定义', 'custom', '<', '小于', 'DateUtil.between(DateUtil.parseDateTime(effectiveDate), DateUtil.parseDateTime(caseDate), DateUnit.DAY)<10', '起保时间小于车辆出险时间10天(出险-起保)', '2021-11-04 17:38:18', '2021-11-04 17:42:04', 'admin', '管理员2', 'admin', '管理员2');
INSERT INTO `rule_condition_config` VALUES (10016, 'custom', '自定义', 'custom', '<', '小于', 'DateUtil.between(DateUtil.parseDateTime(caseDate), DateUtil.parseDateTime(expireDate), DateUnit.DAY)<10', '车辆出险时间小于终保时间10天（终保-出险）', '2021-11-04 17:40:59', '2021-11-04 17:42:39', 'admin', '管理员2', 'admin', '管理员2');

-- ----------------------------
-- Table structure for rule_drools
-- ----------------------------
DROP TABLE IF EXISTS `rule_drools`;
CREATE TABLE `rule_drools`  (
  `business_id` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `content` varchar(10000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'drools内容',
  `foreign_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '外键（应用表）',
  `drools_type` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '执行类型',
  `drools_status` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否有效，1--有效，0--无效',
  `insert_time` datetime(0) NULL DEFAULT NULL COMMENT '插入时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`business_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rule_drools
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
